The “NSC” HTTP Header
---

# Abstract

This document specifies a header for HTTP responses that declares whether the returned content is potentially inappropriate in a given context.
The receiver of the response, such as a web browser, will decide whether to show or discard content based on user setting.

# Status of this Memo

This document defines an Experimental Protocol addition to the HTTP standard for the internet community.
It is published for examination, experimental implementation, and evaluation.
This is a contribution to the RFC Series, independently of any other RFC stream.
The RFC Editor has chosen to publish this document at its discretion and makes no statement about its value for implementation or deployment.
Distribution of this memo is unlimited.

# 1. Introduction

The internet is the most powerful resource for information, communication and collaboration.
It connects people around the world and has become a vivid part in the live of millions of people.

Over the last few decades, the internet has become much more than just a communication platform that offers collections of linked resources.
It has seen many changes and updates to its core protocols and standards to address limitations and open up new possibilities.
At the same time, issues regarding youth protection and protection from inappropriate and harmful content were never addressed at a larger scale.

The protection of children from inappropriate and harmful subjects is considered a fundamental right, yet the internet does not provide a standardized and widely used mechanism to implement such a protection online.
It has been silently tolerated that the internet is not a safe place for a large group of its users.

Without any doubt this must be addressed.
Websites need a standardized mechanism to protect their users.

A possible solution for such a mechanism as extension of the HTTP protocol is introduced in this document.

# 1.1.  Conventions and Definitions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 [RFC2119] [RFC8174] when, and only when, they appear in all capitals, as shown here.

# 2. The “NSC” Header

## 2.1 “Safe” Content

A server SHOULD indicate to a client that content within a response is safe by using the “NSC” response header. 
Safe content SHOULD include the NSC response header with a value of 0. 

```
NSC: 0

```
The client finds the content is “safe” and displays it.
If a server does not include the NSC response header with content, the client MUST display the content as though the server responded with the NSC header set to 0.

## 2.2 “Unsafe” Content

A server SHALL indicate to a client that that content within a response is “unsafe” by using the NSC header with one of several values.
A server MAY set the NSC response header value to 1 to indicate generally “unsafe” content.

```
NSC: 1
```

A server MAY also specify what kind of “unsafe” content it is sending to the client. 
We recommend a standardized content descriptor system as such a system would make it easier for web browsers to filter the kinds of content they do or do not wish to see.
Specifics of the content rating system is beyond the scope of this document, but we suggest broad categories such as adult, drugs, or violence.

### 2.2.1 Caching

“Unsafe” content MAY be cached by clients. However, any cache item that contains data received from “unsafe” responses MUST be cleared as soon as the user enables blocking of unsafe content.
Alternatively, to comply with this specification the client MAY choose to interpret all “unsafe” responses as if they included the following cache-control response headers defined in RFC 7234 Hypertext Transfer Protocol (HTTP/1.1):

```
Cache-Control: no-store, no-transform
```

### 2.2.2 Intermediaries

Any intermediary, such as a proxy server or a VPN (virtual private network), MUST forward any response without changing the NSC header value.
For example, the following response was sent to a proxy:

```http
HTTP/1.1 200 OK
Content-Length: 0
NSC: 1
X-Forwarded-For: 203.0.113.195
```

The proxy might forward the response like this:

```http
HTTP/1.1 200 OK
Content-Length: 0
NSC: 1
X-Forwarded-For: 203.0.113.195
```
Yet, it MUST NOT forward the response like this:

```http
HTTP/1.1 200 OK
Content-Length: 0
NSC: 0
X-Forwarded-For: 203.0.113.195
```

### 2.2.3 WebSockets

If a WebSocket server chooses to accept the incoming connection, the server MUST include the NSC header in its response to the client if it will send any “unsafe” content through the established connection.
NSC information conveyed in the handshake from the server MUST follow section 3.2 of this document.
If the client is set to disallow “unsafe content” the client MUST finish the opening handshake, and immediately begin the WebSocket closing handshake and terminate the session (RFC6455 Section 7).
